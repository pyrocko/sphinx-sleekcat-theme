import os
from sphinx.domains import python as pdom
from sphinx import addnodes
from docutils import nodes

__version__ = "0.1.0"


class GAttributePyAttribute(pdom.PyAttribute):
    def get_signature_prefix(self, sig):
        return [nodes.Text(u'\u2666'), addnodes.desc_sig_space()]


pdom.PythonDomain.object_types['gattribute'] = pdom.ObjType(
    GAttributePyAttribute)
pdom.PythonDomain.directives['gattribute'] = GAttributePyAttribute
pdom.PythonDomain.roles['gattr'] = pdom.PyXRefRole()


def get_html_theme_path():
    return os.path.abspath(os.path.join(os.path.dirname(__file__), 'themes'))
